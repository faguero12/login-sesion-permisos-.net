﻿namespace Violetta.Security
{
    public static class Permisos
    {
        /*****************************************************/
        /*** INICIO - PERMISOS FUNCIONALIDAD USUARIO FINAL ***/
        /*****************************************************/

        /*PERMISO PARA LOGOUT Y PANEL*/
        public const string LOGOUT = "LOGOUT";
        public const string PANEL = "PANEL";

    }
}
