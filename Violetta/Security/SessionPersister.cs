﻿using ViolettaDB.Models;
using System.Web;
using System;

namespace Violetta.Security
{
    public static class SessionPersister
    {
        static string usernameSessionvar = "username";

        static string usuarioSessionvar = "usuario";

        public static string Username
        {
            get
            {
                if (HttpContext.Current == null)
                    return string.Empty;

                var sessionVar = HttpContext.Current.Session[usernameSessionvar];

                if (sessionVar != null)
                    return sessionVar as string;

                return null;
            }
            set
            {
                HttpContext.Current.Session[usernameSessionvar] = value;
            }
        }


        public static Usuario Usuario
        {
            get
            {

                if (HttpContext.Current == null)
                    return null;

                var sessionVar = HttpContext.Current.Session[usuarioSessionvar];

                if (sessionVar != null)
                    return sessionVar as Usuario;

                return null;
            }
            set
            {
                HttpContext.Current.Session[usuarioSessionvar] = value;
            }

        }

        public static void clear()
        {
            Username = string.Empty;
            Usuario = null;

        }
    }
}