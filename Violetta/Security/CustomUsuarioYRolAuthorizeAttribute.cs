﻿using ViolettaDB.DAL;
using ViolettaDB.Models;
using System.Web.Mvc;
using System.Web.Routing;
using Violetta.Security;

namespace Violetta.Security
{
    public class CustomUsuarioYRolAuthorizeAttribute : AuthorizeAttribute
    {
        protected ViolettaContext db = new ViolettaContext();

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (Roles == "LOGOUT")
            {

            }
            //else if (Roles == "PANEL")
            //{
            //    //si el usuario no cambio nunca la password lo redirige a cambio contraseña
            //    if (SessionPersister.Usuario != null && SessionPersister.Usuario.cambioPassword)
            //    {
            //        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "CambiarContrasenia" }));

            //    }

            //}
            else
            {
                //si el usuario no cambio nunca la password lo redirige a cambio contraseña
                //if (SessionPersister.Usuario != null && SessionPersister.Usuario.cambioPassword)
                //{
                //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login", action = "CambiarContrasenia" }));

                //}
                //else
                //{

                    //Si no está el usuario en Session redirecciona a página Home
                    if (SessionPersister.Usuario == null)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "Index" }));

                    }
                    else if (string.IsNullOrEmpty(SessionPersister.Usuario.username))
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "Index" }));
                    }
                    else
                    {
                        //Usuario user = db.Usuarios.Where(u => u.usuarioID.Equals(SessionPersister.Username)).Include(u => u.Perfiles.Select(p => p.Permisos)).Include(u => u.Clientes).SingleOrDefault();
                        //Se reemplaza linea anterior por la siguiente,
                        //De lo contrario, por más que no tuviera permiso a funcionalidad, escribiendo URL en sitio subido a Azure (ej. /Facturado) a veces lo dejaba entrar. 
                        //También se agregó en web.config: <remove name="RoleManager" />
                        Usuario user = (Usuario)SessionPersister.Usuario;

                        CustomPrincipal mp = new CustomPrincipal(user);

                        //Chequeo que el usuario tenga el rol/permiso

                        if (!mp.IsInRole(Roles))
                        {
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "AccessDenied", action = "Index" }));
                        }

                    }
                //}

            }
        }
    }

}