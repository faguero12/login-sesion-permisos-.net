﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Violetta.Security;
using ViolettaDB.Models;

namespace Violetta.Security
{
    public class CustomCuentaUsuarioYRolAuthorizeAttribute : CustomUsuarioYRolAuthorizeAttribute
    {

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            Usuario user = db.Usuarios.Where(u => u.username.Equals(SessionPersister.Username)).Include(u => u.Perfiles.Select(p => p.Permisos)).SingleOrDefault();
            CustomPrincipal mp = new CustomPrincipal(user);
        }
    }
}