﻿using ViolettaDB.DAL;
using ViolettaDB.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace Violetta.Security
{
    public class CustomPrincipal : IPrincipal
    {
        private ViolettaContext db = new ViolettaContext();

        private Usuario Usuario;

        public CustomPrincipal(Usuario usuario)
        {

            if (usuario != null)
            {
                this.Usuario = usuario;
                this.Identity = new GenericIdentity(usuario.username);
            }
        }


        public IIdentity Identity
        {
            get;
            set;
        }

        public bool IsInRole(string role)
        {
            if (string.IsNullOrEmpty(role)) //Si no pide rol el controller / action, retornar true, por ejemplo para ver Selección de Cuenta/cliente no se requiere rol
                return true;
            else
            {

                //Usuario usuario = db.Usuarios.Include("Perfiles").Where(x => x.id == Usuario.id).First();

                //var roles = role.Split(new char[] { ',' });

                //ICollection<Perfil> perfiles = usuario.Perfiles;

                ////Busco si tiene algun perfil con alguno de los roles requeridos
                //return roles.Any(r => perfiles.Any(perf => perf.Permisos.Any(perm => perm.nombre == r)));
                if (this.Usuario == null)
                {
                    return false;
                }

                var roles = role.Split(new char[] { ',' });

                ICollection<Perfil> perfiles = this.Usuario.Perfiles;

                //Busco si tiene algun perfil con alguno de los roles requeridos
                return roles.Any(r => perfiles.Any(perf => perf.Permisos.Any(perm => perm.nombre == r)));



            }
        }
    }
}