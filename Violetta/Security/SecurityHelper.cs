﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViolettaDB.Models;

namespace Violetta.Security
{
    [CustomUsuarioYRolAuthorize()] //No se requiere rol, solo que esté logueado el usuario
    public class SecurityHelper
    {

        /**
            * Devuelve:
            *  true, si el usuario logueado tiene al menos 1 permiso de admin,
            *  false, caso contrario
        **/
        //public static bool userLoggedHasAtLeastOneAdminPermission()
        //{

        //    Usuario user = (Usuario)SessionPersister.Usuario;

        //    CustomPrincipal mp = new CustomPrincipal(user);

        //    /********************************************/
        //    /*** Permisos GESTION DE USUARIOS - ADMIN ***/
        //    /********************************************/
        //    bool result = false;


        //    if (mp.IsInRole(Permisos.USUARIOS_CONSULTA) ||  //Chequeo que el usuario tenga el rol/permiso para Consultar Usuarios
        //        mp.IsInRole(Permisos.LOG_ERRORES_CONSULTA)              //Chequeo que el usuario tenga el rol/permiso para acceder y gestionar Idiomas y Textos
        //       )
        //    {
        //        result = true;
        //    }


        //    return result;

        //}

    }
}