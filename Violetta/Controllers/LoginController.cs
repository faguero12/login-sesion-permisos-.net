﻿using Violetta.Security;
using ViolettaDB.DAL;
using ViolettaDB.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ViolettaUtils.CryptoProvider;
using System.Configuration;

namespace Violetta.Controllers
{
    public class LoginController : Controller
    {
        private ViolettaContext db = new ViolettaContext();

        // GET: Login
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (Request.UrlReferrer != null)
            {
                string path = Request.UrlReferrer.LocalPath.ToLower();
                if (path == "/" || path == "/home" || path == "/home/index" || path == "/login/recuperarcontrasenia" || path == "/accountconfirmation")
                {
                    Session["UltimaPagina"] = "/Panel";
                }
                else
                {
                    Session["UltimaPagina"] = Request.UrlReferrer.PathAndQuery;
                }
            }
            else
            {
                Session["UltimaPagina"] = "/Panel";
            }

            return View();
        }


        [HttpPost]
        public ActionResult Index(Usuario Usuario)
        {
            try
            {

                if (Usuario.username == null || Usuario.username == "" || Usuario.password == null || Usuario.password == "")
                {
                    ViewBag.error = "Por favor complete los campos.";
                    return View("Index", Usuario);
                }

                //Busco si el usuario por username ingresado existe
                var usuarioDB = db.Usuarios.Where(x => x.username == Usuario.username).FirstOrDefault();
                if (usuarioDB == null)
                {
                    ViewBag.error = "No se ha encontrado el Usuario ingresado.";
                    return View("Index");
                }

                string encryptedPassword = !String.IsNullOrEmpty(Usuario.password) ? CryptoService.Encrypt(Usuario.password) : "";

                var obj = db.Usuarios.Where(u => (u.username.Equals(Usuario.username) && u.password == encryptedPassword)).FirstOrDefault();

                if (obj == null)
                {

                    ViewBag.error = "La Contraseña ingresada es incorrecta.";

                    return View("Index");
                }
                else
                {
                    SessionPersister.Username = obj.username;
                    SessionPersister.Usuario = obj;
                    db.SaveChanges();

                    if (Session["UltimaPagina"] != null)
                    {
                        return Redirect(Session["UltimaPagina"].ToString());
                    }
                    else
                    {
                        return RedirectToAction("Index", "Panel");
                    }
                }
            }
            catch (Exception e)
            {
                //LogService.Error(System.Web.HttpContext.Current.Request.UserHostAddress, SessionPersister.Username, "Login", "Index", e.Message);
                ViewBag.error = "Ocurrio un error inesperado, por favor intentelo mas tarde";
            }
            return View("Index", Usuario);
        }


        [CustomCuentaUsuarioYRolAuthorize(Roles = Permisos.LOGOUT)] //No se requiere rol para hacer logout, sólo se valida que la sesión sea válida
        public ActionResult Logout()
        {
            string direccionIP = System.Web.HttpContext.Current.Request.UserHostAddress;
            SessionPersister.clear();
            return RedirectToAction("Index", "Home");
        }


    }
}