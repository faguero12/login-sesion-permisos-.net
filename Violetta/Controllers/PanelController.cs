﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Violetta.Security;
using ViolettaDB.DAL;

namespace Violetta.Controllers
{
    public class PanelController : Controller
    {
        // GET: Panel
        [CustomCuentaUsuarioYRolAuthorize(Roles = Permisos.PANEL)]
        public ActionResult Index()
        {
            string usuario = SessionPersister.Username;

            if (!String.IsNullOrEmpty(usuario))
            {
                using (ViolettaContext objIGSA = new ViolettaContext())
                {

                    if (Request.UrlReferrer != null && Request.UrlReferrer.LocalPath == "/Login")
                    {
                        ViewBag.login = "Ok";
                    }

                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}