﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViolettaDB.Models
{
    public class Usuario
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Usuario")]
        public string username { get; set; }

        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^.*(?=.*[!@#$%^&*\(\)_\-+=]).*$", ErrorMessage = "Requerido: números, símbolos y mayúsculas.")]
        [StringLength(120, MinimumLength = 8, ErrorMessage = "Mínimo 8 caracteres")]
        public string password { get; set; }

        public virtual ICollection<Perfil> Perfiles { get; set; }
    }
}
