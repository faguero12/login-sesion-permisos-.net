﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ViolettaDB.Models
{
    [Table("Permisos")]
    public class Permiso
    {

        [Key]
        [Display(Name = "Código")]
        public int id { get; set; }

        [Display(Name = "Permiso")]
        [Required]
        public string nombre { get; set; }

        public virtual ICollection<Perfil> perfiles { get; set; } 
    }
}
