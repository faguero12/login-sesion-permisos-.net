﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ViolettaDB.Models
{
    [Table("Perfiles")]
    public class Perfil
    {

        public Perfil()
        {
            this.Permisos = new List<Permiso>();
            this.Usuarios = new List<Usuario>();
        }

        [Key]
        public int id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string nombre { get; set; }

        public virtual ICollection<Permiso> Permisos { get; set; } 
        public virtual ICollection<Usuario> Usuarios { get; set; } 
    }
}
