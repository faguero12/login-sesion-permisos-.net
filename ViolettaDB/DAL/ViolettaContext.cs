﻿using ViolettaDB.Models;
using System.Data.Entity;

namespace ViolettaDB.DAL
{
    public class ViolettaContext : DbContext
    {
        public ViolettaContext() : base("ViolettaContext")
        {

        }

        public System.Data.Entity.DbSet<ViolettaDB.Models.Usuario> Usuarios { get; set; }
        public System.Data.Entity.DbSet<ViolettaDB.Models.Permiso> Permisos { get; set; }
        public System.Data.Entity.DbSet<ViolettaDB.Models.Perfil> Perfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
