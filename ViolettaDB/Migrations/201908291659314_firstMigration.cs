namespace ViolettaDB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Perfiles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Permisos",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        username = c.String(),
                        password = c.String(maxLength: 120),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PermisoPerfils",
                c => new
                    {
                        Permiso_id = c.Int(nullable: false),
                        Perfil_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Permiso_id, t.Perfil_id })
                .ForeignKey("dbo.Permisos", t => t.Permiso_id, cascadeDelete: true)
                .ForeignKey("dbo.Perfiles", t => t.Perfil_id, cascadeDelete: true)
                .Index(t => t.Permiso_id)
                .Index(t => t.Perfil_id);
            
            CreateTable(
                "dbo.UsuarioPerfils",
                c => new
                    {
                        Usuario_id = c.Int(nullable: false),
                        Perfil_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Usuario_id, t.Perfil_id })
                .ForeignKey("dbo.Usuarios", t => t.Usuario_id, cascadeDelete: true)
                .ForeignKey("dbo.Perfiles", t => t.Perfil_id, cascadeDelete: true)
                .Index(t => t.Usuario_id)
                .Index(t => t.Perfil_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsuarioPerfils", "Perfil_id", "dbo.Perfiles");
            DropForeignKey("dbo.UsuarioPerfils", "Usuario_id", "dbo.Usuarios");
            DropForeignKey("dbo.PermisoPerfils", "Perfil_id", "dbo.Perfiles");
            DropForeignKey("dbo.PermisoPerfils", "Permiso_id", "dbo.Permisos");
            DropIndex("dbo.UsuarioPerfils", new[] { "Perfil_id" });
            DropIndex("dbo.UsuarioPerfils", new[] { "Usuario_id" });
            DropIndex("dbo.PermisoPerfils", new[] { "Perfil_id" });
            DropIndex("dbo.PermisoPerfils", new[] { "Permiso_id" });
            DropTable("dbo.UsuarioPerfils");
            DropTable("dbo.PermisoPerfils");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Permisos");
            DropTable("dbo.Perfiles");
        }
    }
}
